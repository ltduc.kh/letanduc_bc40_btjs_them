const removeWhiteSpace = e=>e.replace(/\s/g, "")
  , hasValue = e=>"" !== removeWhiteSpace(e)
  , isNum = e=>!isNaN(Number(removeWhiteSpace(e)))
  , isInt = e=>(e = Number(removeWhiteSpace(e)),
!!Number.isInteger(e))
  , isPositive = e=>(e = Number(removeWhiteSpace(e))) >= 0
  , dateIsValid = e=>{
    // return !!moment(e, "DD.MM.YYYY").dateIsValid()
}
   validateYear = e=>e >= 1920 || (alert("Năm cần lớn hơn 1920"),
!1)
  ,validateMonth = e=>e >= 1 && e <= 12;
  
document.getElementById("btnTomorrow").onclick = function() {
    var e = document.getElementById("inputDay").value
      , t = document.getElementById("inputMonth").value
      , n = document.getElementById("inputYear").value
      , a = "";
    let i = hasValue(e) && isNum(e) && isPositive(e) && isInt(e) && hasValue(t) && isNum(t) && isPositive(t) && isInt(t) && hasValue(n) && isNum(n) && isPositive(n) && isInt(n) && dateIsValid(`${e}.${t}.${n}`);
    (i &= validateYear(Number(n))) ? ((a = new Date(`${n}-${t}-${e}`)).setDate(a.getDate() + 1),
    document.getElementById("txtDate").innerHTML = a.toLocaleDateString("en-GB")) : (document.getElementById("txtDate").innerHTML = "",
    alert("Dữ liệu không hợp lệ"))
}
document.getElementById("btnYesterday").onclick = function() {
    var e = document.getElementById("inputDay").value
      , t = document.getElementById("inputMonth").value
      , n = document.getElementById("inputYear").value
      , a = "";
    let i = hasValue(e) && isNum(e) && isPositive(e) && isInt(e) && hasValue(t) && isNum(t) && isPositive(t) && isInt(t) && hasValue(n) && isNum(n) && isPositive(n) && isInt(n) && dateIsValid(`${e}.${t}.${n}`);
    (i &= validateYear(Number(n))) ? ((a = new Date(`${n}-${t}-${e}`)).setDate(a.getDate() - 1),
    document.getElementById("txtDate").innerHTML = a.toLocaleDateString("en-GB")) : (document.getElementById("txtDate").innerHTML = "",
    alert("Dữ liệu không hợp lệ"))
}
document.getElementById("btnCalcDay").onclick = function() {
    var e = document.getElementById("inputMonth2").value
      , t = document.getElementById("inputYear2").value
      , n = !1
      , a = 0;
    let i = hasValue(e) && isNum(e) && isPositive(e) && isInt(e) && hasValue(t) && isNum(t) && isPositive(t) && isInt(t);
    if (i &= validateYear(Number(t)) && validateMonth(Number(e))) {
        switch (e = Number(e),
        ((t = Number(t)) % 4 == 0 && t % 100 != 0 || t % 400 == 0) && (n = !0),
        e) {
        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12:
            a = 31;
            break;
        case 4:
        case 6:
        case 9:
        case 11:
            a = 30;
            break;
        case 2:
            a = n ? 29 : 28;
            break;
        default:
            a = 0
        }
        document.getElementById("txtCalcDay").innerHTML = "Tháng " + e + " Năm " + t + " Có " + a + " Ngày"
    } else
        document.getElementById("txtCalcDay").innerHTML = "",
        alert("Dữ liệu không hợp lệ")
}
document.getElementById("btnReadInt").addEventListener("click", function() {
    var e = document.getElementById("inputReadInt").value
      , t = Math.floor(e / 100)
      , n = Math.floor(e % 100 / 10)
      , a = e % 100 % 10
      , i = ""
      , u = ""
      , m = ""
      , l = "";
    if (!(hasValue(e) && isNum(e) && isInt(e) && Number(e) > 99 && Number(e) < 1e3))
        return document.getElementById("txtReadInt").innerHTML = "",
        void alert("Dữ liệu không hợp lệ");
    switch (t) {
    case 1:
        i = "Một Trăm ";
        break;
    case 2:
        i = "Hai Trăm ";
        break;
    case 3:
        i = "Ba Trăm ";
        break;
    case 4:
        i = "Bốn Trăm ";
        break;
    case 5:
        i = "Năm Trăm ";
        break;
    case 6:
        i = "Sáu Trăm ";
        break;
    case 7:
        i = "Bảy Trăm ";
        break;
    case 8:
        i = "Tám Trăm ";
        break;
    case 9:
        i = "Chín Trăm ";
        break;
    default:
        alert("số hàng trăm không xác định được")
    }
    switch (n) {
    case 0:
        u = "Lẻ ";
        break;
    case 1:
        u = "Mười ";
        break;
    case 2:
        u = "Hai Mươi ";
        break;
    case 3:
        u = "Ba Mươi ";
        break;
    case 4:
        u = "Bốn Mươi ";
        break;
    case 5:
        u = "Năm Mươi ";
        break;
    case 6:
        u = "Sáu Mươi ";
        break;
    case 7:
        u = "Bảy Mươi ";
        break;
    case 8:
        u = "Tám Mươi ";
        break;
    case 9:
        u = "Chín Mươi ";
        break;
    default:
        alert("Số hàng chục không xác định được")
    }
    switch (a) {
    case 0:
        m = "";
        break;
    case 1:
        m = "Một";
        break;
    case 2:
        m = "Hai";
        break;
    case 3:
        m = "Ba";
        break;
    case 4:
        m = "Bốn";
        break;
    case 5:
        m = "Năm";
        break;
    case 6:
        m = "Sáu";
        break;
    case 7:
        m = "Bảy";
        break;
    case 8:
        m = "Tám";
        break;
    case 9:
        m = "Chín";
        break;
    default:
        alert("Số hàng đơn vị không xác định được.")
    }
    0 == a && 0 == n && (u = ""),
    isPositive(e) || (l = "âm "),
    document.getElementById("txtReadInt").innerHTML = l + i + u + m
})
document.getElementById("btnSearch").addEventListener("click", function() {
    var e = document.getElementById("inputName1").value
      , t = document.getElementById("inputX1").value
      , n = document.getElementById("inputY1").value
      , a = document.getElementById("inputName2").value
      , i = document.getElementById("inputX2").value
      , u = document.getElementById("inputY2").value
      , m = document.getElementById("inputName3").value
      , l = document.getElementById("inputX3").value
      , c = document.getElementById("inputY3").value
      , s = document.getElementById("inputX4").value
      , o = document.getElementById("inputY4").value;
    if (!(hasValue(e) && hasValue(a) && hasValue(m) && isPositive(t) && isPositive(i) && isPositive(l) && isPositive(s) && isPositive(n) && isPositive(u) && isPositive(c) && isPositive(o)))
        return document.getElementById("txtSearch").innerHTML = "",
        void alert("Dữ liệu không hợp lệ");
    var r = Math.pow(s - t, 2) + Math.pow(o - n, 2)
      , d = Math.sqrt(r);
    console.log(d);
    var h = Math.pow(s - i, 2) + Math.pow(o - u, 2)
      , g = Math.sqrt(h);
    console.log(g);
    var v = Math.pow(s - l, 2) + Math.pow(o - c, 2)
      , I = Math.sqrt(v);
    console.log(I);
    var b = "";
    b = d > g && d > I ? e : g > d && g > I ? a : m,
    document.getElementById("txtSearch").innerHTML = "Sinh Viên Xa Trường Nhất: " + b
});
